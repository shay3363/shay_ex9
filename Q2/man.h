#pragma once
#include<iostream>
using namespace std;
class man
{
private:
	double _height;
public:
	man(double height);
	man();
	~man();
	bool operator<(const man& other) const;
	bool operator>(const man& other) const;
	man& operator=(const man& other);
	bool operator==(const man& other) const;
	friend ostream& operator<< (ostream& stream, const man& myMan);
};