#pragma once
#include <iostream>
template <class T>
int compare(T first, T second)
{
	int retVal = 1;
	if (first > second)
	{
		retVal = -1;
	}
	else if (first == second)
	{
		retVal = 0;
	}
	return retVal;
}
template <class T>
void swap(T& firstPtr, T& secondPtr)
{
	T temp = firstPtr;
	firstPtr = secondPtr;
	secondPtr = temp;
}
template <class T>
void bubbleSort(T arr[], int size)
{
	int i = 0;
	int k = 0;
	for (i = 0; i < size - 1; i++)
	{
		for(k=0;k<size-i-1;k++)
		{
			if(arr[k] > arr[k+1])
			{ 
				swap(arr[k], arr[k + 1]);
			}
		}
	}
}
template <class T>
void printArray(T arr[], int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}