#include "man.h"
man::man(double height)
{
	this->_height = height;
}
man::man() : man(0)
{

}
man::~man()
{

}
bool man::operator<(const man& other) const
{
	bool flag = false;
	if (this->_height < other._height)
	{
		flag = true;
	}
	return flag;
}
bool man::operator>(const man& other) const
{
	bool flag = false;
	if (this->_height > other._height)
	{
		flag = true;
	}
	return flag;
}
bool man::operator==(const man& other) const
{
	bool flag = false;
	if (this->_height == other._height)
	{
		flag = true;
	}
	return flag;
}
man& man::operator=(const man& other) 
{
	this->_height = other._height;
	return *this;
	
}
ostream& operator<< (ostream& stream, const man& myMan)
{
	stream << myMan._height;
	return stream;
}