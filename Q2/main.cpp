#include "functions.h"
#include "man.h"
#include <iostream>


int main() {
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	std::cout << "__________________________________________________________________________________" << std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 't') << std::endl;
	std::cout << compare<char>('c', 'b') << std::endl;
	std::cout << compare<char>('d', 'd') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arrChar_size = 5;
	char charArr[arrChar_size] = { 't', '3', 'o', '2', 'n' };
	bubbleSort<char>(charArr, arrChar_size);
	for (int i = 0; i < arrChar_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arrChar_size);
	std::cout << std::endl;
	std::cout << "__________________________________________________________________________________" << std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<man>(man(1), man(9)) << std::endl;
	std::cout << compare<man>(man(1), man(0.2)) << std::endl;
	std::cout << compare<man>(man(1), man(1)) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arrMan_size = 5;
	man manArr[arrMan_size] = { man(1), man(0.6),man(9),man(8),man(3) };
	bubbleSort<man>(manArr, arrMan_size);
	for (int i = 0; i < arrMan_size; i++) {
		std::cout << manArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<man>(manArr, arrMan_size);
	std::cout << std::endl;
	system("pause");
	return 1;
}