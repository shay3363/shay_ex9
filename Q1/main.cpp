#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	//string textTree = "BSTData.txt";
	//printTreeToFile(bs, textTree);
	//system("BinaryTree.exe");
	cout << "Tree height: " << bs->getHeight() << endl;
	bs->printNodes();
	cout << "is 5 in tree?" << bs->search("5") << endl;
	cout << "is 7 in tree?" <<  bs->search("7") << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	system("pause");
	delete bs;

	return 0;
}

