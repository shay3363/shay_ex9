#include "BSNode.h"
BSNode::BSNode(string data)
{
	this->_data = data;
	this->_count = 1;
}
BSNode::BSNode(const BSNode& other)
{
	this->_data = other._data;
	this->_count = other._count;
	if (other._right)
	{
		this->_right = new BSNode(*other._right);
	}
	else
	{
		this->_right = 0;
	}
	if (other._left)
	{
		this->_left = new BSNode(*other._left);
	}
	else
	{
		this->_left = 0;
	}
}
BSNode::~BSNode()
{
	if (this->_left)
	{
		delete _left;
	}
	if (this->_right)
	{
		delete _right;
	}
}
void BSNode::insert(string value)
{
	if (value > this->_data)
	{
		if (this->_right)
		{
			this->_right->insert(value);
		}
		else
		{
			this->_right = new BSNode(value);
		}
	}
	else if(value < this->_data)
	{
		if (this->_left)
		{
			this->_left->insert(value);
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
	else
	{
		this->_count++;
	}
}
BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other._data;
	this->_count = other._count;
	this->_left = new BSNode(*other._left);
	this->_right = new BSNode(*other._right);
	return *this;
}
bool BSNode::isLeaf() const
{
	bool flag = true;
	if (!this->_right && !this->_left)
	{
		flag = false;
	}
	return flag;
}
string BSNode::getData() const
{
	return this->_data;
}
BSNode* BSNode::getRight() const
{
	return this->_right;
}
BSNode* BSNode::getLeft() const
{
	return this->_left;
}
bool BSNode::search(string val) const
{
	bool flag = false;
	if (val > this->_data)
	{
		if (this->_right)
		{
			flag = this->_right->search(val);
		}
		else
		{
			flag = false;
		}
	}
	else if (val < this->_data)
	{
		if (this->_left)
		{
			flag = this->_left->search(val);
		}
		else
		{
			flag = false;
		}
	}
	else
	{
		flag = true;
	}
	return flag;
}
int BSNode::getHeight() const
{
	int heightLeft = 0;
	int heightRight = 0;
	int biggerHeight = 0;
	if (this->_left)
	{
		heightLeft = 1 + this->_left->getHeight();
	}
	if (this->_right)
	{
		heightRight = 1 + this->_right->getHeight();
	}

	biggerHeight = heightLeft;
	if (biggerHeight < heightRight)
	{
		biggerHeight = heightRight;
	}
	return biggerHeight;
}
int BSNode::getDepth(const BSNode& root) const
{
	int depth = 0;
	int temp = 0;
	if (this->_data > root._data)
	{
		if (root._right)
		{
			temp = getDepth(*root._right);
			if (temp != -1)
			{
				depth = 1 + temp;
			}
			else
			{
				depth = temp;
			}
		}
		else
		{
			depth = -1;
		}
	}
	else if (this->_data < root._data)
	{
		if (root._left)
		{
			temp = getDepth(*root._left);
			if (temp != -1)
			{
				depth = 1 + temp;
			}
			else
			{
				depth = temp;
			}
		}
		else
		{
			depth = -1;
		}
	}
	return depth;
}
void BSNode::printNodes() const
{
	if (this)
	{
		this->_left->printNodes();
		cout <<  this->_data << " " << this->_count << endl;
		this->_right->printNodes();
	}
}