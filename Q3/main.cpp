#include <iostream>
#include <string>
#include "BSNode.h"
#define SIZE 15
using namespace std;
int main(void)
{
	int nums[SIZE] = { 2,9,1,-8,132,23874,421,-123,-77,329,0,324,8432,-12831,16 };
	string myStrings[SIZE] = { "1eqwd","132fsd","6034","adsda","uutae","znvk","vinnv","iqeu","eqem","jij","quwe","czcz","3i192","ubbyvt","18" };
	BSNode<int> intTree = BSNode<int>(nums[0]);
	cout << nums[0] << " ";
	BSNode<string> strTree = BSNode<string>(myStrings[0]);
	int i = 0;
	for (i = 1; i < SIZE; i++)
	{
		cout << nums[i] << " ";
		intTree.insert(nums[i]);
	}
	cout << endl;
	cout << myStrings[0] << " ";
	for (i = 1; i < SIZE; i++)
	{
		cout << myStrings[i] << " ";
		strTree.insert(myStrings[i]);
	}
	intTree.printNodes();
	cout << "____________________________________________________________________________________________________________________________" << endl;
	strTree.printNodes();
	system("pause");
	return 0;
}